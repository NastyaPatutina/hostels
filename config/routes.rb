Rails.application.routes.draw do
  get 'welcome/index'

  post '/countries/:country_id/regions', to: 'regions#createId'
  post '/regions/:region_id/cities', to: 'cities#createId'
  post '/cities/:city_id/hostels', to: 'hostels#createId'
 
  resources :countries do
  	resources :regions, only: [:index, :new, :destroy] do
    end
  end

  resources :regions do
  	resources :cities, only: [:index, :new, :destroy]
  end

  resources :cities do
    resources :hostels, only: [:index, :new, :destroy]
  end
  
  resources :hostels
 
  root 'welcome#index'
end
