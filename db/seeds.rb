# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

arrayCountries = Array.new 
5.times do |i|
  arrayCountries[i] = Country.create(name: "Country##{i}")
end

arrayRegions = Array.new 
15.times do |i|
  arrayRegions [i] = Region.create(name: "Region##{i}", country_id: arrayCountries[i%5].id)
end

arrayCities = Array.new 
30.times do |i|
  arrayCities[i] = City.create(name: "City##{i}", region_id: arrayRegions[i%15].id)
end

60.times do |i|
  Hostel.create(name: "Hostel##{i}", city_id: arrayCities[i%30].id)
end