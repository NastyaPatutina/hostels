class CitiesController < ApplicationController
	def index
		@cities = City.all
	end

	def show
		@city = City.find(params[:id])
	end

	def new
		@city = City.new
	end

	def edit
		@city = City.find(params[:id])
	end

	def update
		@city = City.find(params[:id])

		if @city.update(city_params)
			redirect_to @city
		else
			render 'edit'
		end
	end

	def create
		if (city_params[:region_id] != "") then
			@region = Region.find(city_params[:region_id])
			@city = @region.cities.create(city_params)

			redirect_back(fallback_location: root_path)
		end
	end

	def createId
		@region = Region.find(params[:region_id])
		@city = @region.cities.create(city_params)

		redirect_back(fallback_location: root_path)
	end

	def destroy
		@city = City.find(params[:id])
		if (@city.hostels.count == 0) then
			@city.destroy
			redirect_back(fallback_location: root_path)
		end

	end

	private
	def city_params
		params.require(:city).permit(:region_id, :name)
	end
end
