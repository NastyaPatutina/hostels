class HostelsController < ApplicationController

	def index
		@hostels = Hostel.all
	end

	def show
		@hostel = Hostel.find(params[:id])
	end

	def new
		@hostel = Hostel.new
	end

	def edit
		@hostel = Hostel.find(params[:id])
	end

	def update
		@hostel = Hostel.find(params[:id])

		if @hostel.update(hostel_params)
			redirect_to @hostel
		else
			render 'edit'
		end
	end

	def create
		if (hostel_params[:city_id] != "") then
			@city = City.find(hostel_params[:city_id])
			@hostel = @city.hostels.create(hostel_params)

			redirect_back(fallback_location: root_path)
		end
	end

	def createId
		@city = City.find(params[:city_id])
		@hostel = @city.hostels.create(hostel_params)

		redirect_back(fallback_location: root_path)
	end

	def destroy
		@hostel = Hostel.find(params[:id])
		@hostel.destroy

		redirect_back(fallback_location: root_path)
	end

	private
	def hostel_params
		params.require(:hostel).permit(:city_id, :name)
	end
end
