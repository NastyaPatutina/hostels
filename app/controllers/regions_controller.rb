class RegionsController < ApplicationController

	def index
		@regions = Region.all
	end

	def show
		@region = Region.find(params[:id])
	end

	def new
		@region = Region.new
	end

	def edit
		@region = Region.find(params[:id])
	end

	def update
		@region = Region.find(params[:id])

		if @region.update(region_params)
			redirect_to @region
		else
			render 'edit'
		end
	end

	def create
		if (region_params[:country_id] != "") then
			@country = Country.find(region_params[:country_id])
			@region = @country.regions.create(region_params)

			redirect_back(fallback_location: root_path)
		end
	end

	def createId

		@country = Country.find(params[:country_id])			
		@region = @country.regions.create(region_params)

		redirect_back(fallback_location: root_path)
	end

	def destroy
		@region = Region.find(params[:id])
		if (@region.cities.count == 0) then
			@region.destroy
			redirect_back(fallback_location: root_path)
		end

	end

	private
	def region_params
		params.require(:region).permit(:country_id, :name)
	end
end
