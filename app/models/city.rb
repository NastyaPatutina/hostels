class City < ApplicationRecord
  belongs_to :region
  has_many :hostels, dependent: :destroy
end
