Select  cities.name, COUNT(*) from 
	hostels_development.cities inner join hostels_development.hostels
    on 
	hostels.city_id = cities.id GROUP BY cities.name
Union 
	Select  regions.name, COUNT(*) from ((
	hostels_development.regions inner join hostels_development.cities
    on 
	cities.region_id = regions.id )
    inner join hostels_development.hostels 
    on 
	hostels.city_id = cities.id )
    GROUP BY regions.name
Union 
	Select  countries.name, COUNT(*) from (( (
	hostels_development.countries inner join hostels_development.regions
    on 
	regions.country_id = countries.id)
    inner join hostels_development.cities
    on 
	cities.region_id = regions.id)
    inner join hostels_development.hostels 
    on 
	hostels.city_id = cities.id )
    GROUP BY countries.name;
 